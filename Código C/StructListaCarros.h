#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct AtributosCarros{
	char modelo[30];
	char ano[10];
	int codigo;
	int numero_portas;
	char cor[10];
	char motor[30];
};
typedef struct AtributosCarros infoCarros;
struct _noCarros{
	infoCarros informacao;
	struct _noCarros *prox;
	struct _noCarros *ant;
};
typedef struct _noCarros noCarros;
struct cabCarros{
	int qtde;
	noCarros *inicio;
	noCarros *final;
};
typedef struct cabCarros listaCarros;
