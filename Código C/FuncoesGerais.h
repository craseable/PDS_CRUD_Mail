#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
/*le um conjunto de caracteres digitados , e salva em um array de char */
void LerString(char *Nome)
{
    char c;
    int i=0;
    do
    {
        c = getchar();
	if(c == '\n'){
		break;
	}
        Nome[i] = c;
        i++;
    }while(c != '\n');
	Nome[i] = '\0';
}
