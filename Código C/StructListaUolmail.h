#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

struct AtributosUolmail{
	int codigo;
	char nome[30];
	char data[10];
	char sexo[10];
	char telefone[9];
	char senha[20];
	char email[50];
	char padraoPessoa[11];
	char padraoEmpresa[19];
	char pessoaJuridica[30];
	char razaoSocial[30];
	char cnpj[14];
	
};
typedef struct AtributosUolmail infoUolmail;
struct _noUolmail{
	infoUolmail informacao;
	struct _noUolmail *prox;
	struct _noUolmail *ant;

};
typedef struct _noUolmail noUolmail;
struct cabUolmail{
	int qtde;
	noUolmail *inicio;
	noUolmail *final;
};
typedef struct cabUolmail listaUolmail;
