
#include "StructListaUolmail.h"
#include "FuncoesGeraisUolmail.h"	
#include "FuncoesListaUolmail.h"
#include "FuncoesSalvar_e_RecuperarUolmail.h"
int Uolmail(){
	int escolha;
	listaUolmail *l = (listaUolmail*)malloc(sizeof(listaUolmail));
	l->qtde = 0;
	l->inicio = NULL;
	l->final = NULL;
	RecuperarUolmail(l);
	while(1){
		system("clear");
		printf("\n1-Cadastrar UolMail.\n2-Listar Mails.\n3-Alterar dados cadastrais.\n4-Remover conta.\n5-Voltar ao menu\n\n");
		printf("Escolha uma opção:");
		scanf("%d",&escolha);
		__fpurge(stdin);
		if(escolha == 1){
			printf("\n\n\tDados Cadastrais\n\n");
			printf("Escolha uma opção:\n");
			printf("1 - Pessoa Física\n2 - Pessoa Jurídica\n0 - Voltar a tela anterior\n");
			int opcao;
			scanf("%d",&opcao);
			switch(opcao)
			{
			case 1:
				system("clear");
				l->qtde++;
				InserirListaUolmail(l);
				SalvarUolmail(l);
				RecuperarUolmail(l);
				break;
			case 2:
				system("clear");
				l->qtde++;
				InserirListaJuridica(l);
				SalvarUolmail(l);
				RecuperarUolmail(l);
				break;
			case 0:
				break;
			}
		}
		else if(escolha == 4){
			int opcao;
			printf("Deseja excluir uma pessoa: \n1- Física. \n2- Jurídica.\n");
			scanf("%d",&opcao);
			__fpurge(stdin);
			switch(opcao)
			{
			case 1:
				system("clear");
				
				l->qtde--;
				RemoverUolmail(l);
				SalvarUolmail(l);
				RecuperarUolmail(l);
				break;
			case 2:
				system("clear");
				l->qtde--;
				RemoverJuridico(l);
				SalvarUolmail(l);
				RecuperarUolmail(l);
				break;
			}	
			
		}
		else if(escolha == 3){
			system("clear");
			MudarInfoUolmail(l);
			SalvarUolmail(l);
			RecuperarUolmail(l);
		}
		else if(escolha == 2){
			system("clear");
			ImprimeTodosUolmail(l);
			ImprimeEspecificoUolmail(l);
		}
		else if(escolha == 5){
			break;
		}
	}	
	return 0;
}
