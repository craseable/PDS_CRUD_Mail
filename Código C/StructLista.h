#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

struct Atributos{
	int codigo;
	char nome[30];
	char data[10];
	char sexo[10];
	char telefone[9];
	char senha[20];
	char email[50];
	char padraoPessoa[11];
	char padraoEmpresa[19];
	char pessoaJuridica[30];
	char razaoSocial[30];
	char cnpj[14];
	
};
typedef struct Atributos info;
struct _no{
	info informacao;
	struct _no *prox;
	struct _no *ant;

};
typedef struct _no no;
struct cab{
	int qtde;
	no *inicio;
	no *final;
};
typedef struct cab lista;
