#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
/*Salva o conteudo de uma lista em um arquivo.dat*/
void Salvar(lista *l){
	system("clear");
	char h;
	printf("\n'Deseja salvar alterações? [s] para sim , [n] para não'\n");
	h = getchar();
	__fpurge(stdin);
	if(h == 's'){
	FILE *arq;
	int ret;
	int i = 0;
	char c;
	no *tmp = l->final;
	arq = fopen("ArquivoEsportes.dat","w");
	info infos[l->qtde];
	while(tmp != NULL){
		infos[i] = tmp->informacao;
		tmp = tmp->ant;
		i++;
	}
	if(arq != NULL){
			ret = fwrite(infos,sizeof(info),l->qtde,arq);
			printf("\nForam gravados %d registros\n",ret);
			fclose(arq);
	}
	else
		printf("Erro:abertura no arquivo");
	printf("\n\n'Aperte a tecla [ENTER] para voltar ao menu.'\n");
	c = getchar();
	__fpurge(stdin);
	}
}
/*Le o arquivo.dat que contem as informacoes/atributos e cria uma lista para cada struct*/
void Recuperar(lista *l){
	while(l->inicio != NULL){
		RemoverInicio(l);
	}
	FILE *arq;
	int i = 0,ret;
	info infos[30];
	arq = fopen("ArquivoEsportes.dat","r");
	if(arq != NULL){
		ret = fread(infos,sizeof(info),30,arq);
		l->qtde = ret;
	}
	while(i < l->qtde){
		no *p = (no*)malloc(sizeof(no));
		p->informacao = infos[i];
		p->ant = NULL;
		p->prox = l->inicio;
		l->inicio = p;
		if(l->final == NULL){
			l->final = l->inicio;
		}
		else
			l->inicio->prox->ant = p;
		i++;
	}
}

