
#include "StructListaEsportes.h"
#include "FuncoesGerais.h"	
#include "FuncoesListaEsportes.h"
#include "FuncoesSalvar_e_RecuperarEsportes.h"
int Esportes(){
	int escolha;
	lista *l = (lista*)malloc(sizeof(lista));
	l->qtde = 0;
	l->inicio = NULL;
	l->final = NULL;
	Recuperar(l);
	while(1){
		system("clear");
		printf("\n1-Cadastrar time.\n2-Consultar time.\n3-Alterar informações de time.\n4-Remover time.\n5-Voltar ao menu\n\n");
		printf("Escolha uma opção:");
		scanf("%d",&escolha);
		__fpurge(stdin);
		if(escolha == 1){
			system("clear");
			l->qtde++;
			InserirLista(l);
			Salvar(l);
			Recuperar(l);
		}
		else if(escolha == 4){
			system("clear");
			l->qtde--;
			Remover(l);
			Salvar(l);
			Recuperar(l);
		}
		else if(escolha == 3){
			system("clear");
			MudarInfo(l);
			Salvar(l);
			Recuperar(l);
		}
		else if(escolha == 2){
			system("clear");
			ImprimeTodos(l);
			ImprimeEspecifico(l);
		}
		else if(escolha == 5){
			break;
		}
	}
	return 0;	
}
