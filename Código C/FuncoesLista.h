#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

void InserirLista(lista *l){
	no *p = (no*)malloc(sizeof(no));
	printf("\n\n\tDados Cadastrais\n\n");
	printf("\nNome:");
	__fpurge(stdin);
	LerString(p->informacao.nome);
	__fpurge(stdin);
	p->informacao.codigo = l->qtde;
	printf("\nCodigo do UolMail é: %d\n",p->informacao.codigo);
	printf("\nData de Nascimento:");
	__fpurge(stdin);
	fgets(p->informacao.data,10,stdin);
	printf("\nSexo:");
	__fpurge(stdin);
	fgets(p->informacao.sexo,10,stdin);
	__fpurge(stdin);
	printf("\nTelefone:");
	fgets(p->informacao.telefone,9,stdin);
	__fpurge(stdin);
	printf("\nDigite o email a ser criado\n(por padrão, o final precisa @uolmail.com):");
	fgets(p->informacao.email,50,stdin);
	__fpurge(stdin);
	printf("\nSenha:");
	fgets(p->informacao.senha,20,stdin);
	__fpurge(stdin);
	char confirmaSenha[20];
	printf("\nConfirme a senha:");
	fgets(confirmaSenha,20,stdin);
	__fpurge(stdin);
	int comp = strcmp(confirmaSenha, p->informacao.senha);
	while(comp!=0)
	{
		printf("A Senha não confere! Digite a senha novamente: ");
		fgets(p->informacao.senha,20,stdin);
		__fpurge(stdin);
		char confirmaSenha[20];
		printf("\nConfirme a senha:");
		fgets(confirmaSenha,20,stdin);
		__fpurge(stdin);
		comp = strcmp(confirmaSenha, p->informacao.senha);
	}
	p->prox = l->inicio;
	p->ant = NULL;
	l->inicio = p;
	if(l->final == NULL)
		l->final = l->inicio;
	else
		l->inicio->prox->ant = p;
}
void InserirListaJuridica(lista *l){
	no *p = (no*)malloc(sizeof(no));
	printf("\n\n\tDados Cadastrais\n\n");
	printf("\nPessoa Jurídica: ");
	__fpurge(stdin);
	LerString(p->informacao.pessoaJuridica);
	__fpurge(stdin);
	p->informacao.codigo = l->qtde;
	printf("\nCodigo do UolMail é: %d\n",p->informacao.codigo);
	printf("\nRazão Social: ");
	__fpurge(stdin);
	fgets(p->informacao.razaoSocial,30,stdin);
	strcpy(p->informacao.nome, p->informacao.razaoSocial);
	printf("\nCNPJ :");
	__fpurge(stdin);
	fgets(p->informacao.cnpj,14,stdin);
	__fpurge(stdin);
	printf("\nTelefone:");
	fgets(p->informacao.telefone,9,stdin);
	__fpurge(stdin);
	printf("\nDigite o email a ser criado\n(por padrão, o final precisa ser .empresa@uolmail.com):");
	fgets(p->informacao.email,50,stdin);
	__fpurge(stdin);
	printf("\nSenha:");
	fgets(p->informacao.senha,20,stdin);
	__fpurge(stdin);
	char confirmaSenha[20];
	printf("\nConfirme a senha:");
	fgets(confirmaSenha,20,stdin);
	__fpurge(stdin);
	int comp = strcmp(confirmaSenha, p->informacao.senha);
	while(comp!=0)
	{
		printf("A Senha não confere! Digite a senha novamente: ");
		fgets(p->informacao.senha,20,stdin);
		__fpurge(stdin);
		char confirmaSenha[20];
		printf("\nConfirme a senha:");
		fgets(confirmaSenha,20,stdin);
		__fpurge(stdin);
		comp = strcmp(confirmaSenha, p->informacao.senha);
	}
	p->prox = l->inicio;
	p->ant = NULL;
	l->inicio = p;
	if(l->final == NULL)
		l->final = l->inicio;
	else
		l->inicio->prox->ant = p;
}
void ImprimeTodos(lista *l){
	no *p = l->inicio;
	while(p != NULL){
		printf("--------------------------------------------------------------------------------");
		printf("\n%d  -  %s\n",p->informacao.codigo,p->informacao.nome);
		p = p->prox;
	}
	printf("--------------------------------------------------------------------------------");
	printf("\n");
}


void ImprimeEspecifico(lista *l){
	no *tmp = l->inicio;
	char c[30];
	char n;
	int escolha;
	int opcao;
	printf("Gostaria de pesquisar por um email de: \n1-Pessoa Física. \n2-Pessoa Jurídica.\n");
	printf("Opção:");
	scanf("%d",&opcao); 
	switch(opcao)
	{
	case 1:
		printf("\nGostaria de pesquisar por:\n1-Nome.\n2-Codigo do mail.\n");
		printf("Escolha:");
		scanf("%d",&escolha);
		__fpurge(stdin);
		if(escolha == 1){
			printf("\nDigite o nome da pessoa a ser pesquisada:");
			LerString(c);
			__fpurge(stdin);
			int i = 0;
			int j = 0;
			while(c[i] != '\0'){
				i++;
			}
			int k = 0;
			while(tmp != NULL){
				j = 0;
				while(j < i){
					if(tmp->informacao.nome[k] == c[k])
						k++;
				j++;
				}
				if(k == i){
					printf("\n%d - %s\n",tmp->informacao.codigo,tmp->informacao.nome);
					printf("\nData de Nascimento:%s",tmp->informacao.data);
					printf("\nSexo:%s",tmp->informacao.sexo);
					printf("\n\nTelefone:%s",tmp->informacao.telefone);
					printf("\nSenha:%s",tmp->informacao.senha);
					printf("\nUol Mail:%s",tmp->informacao.email);
					break;
				}
			tmp = tmp->prox;
			}
		}
		else if(escolha == 2){
			int codigo;
			printf("\nDigite o codigo do mail desejado:");
			scanf("%d",&codigo);
			__fpurge(stdin);
			while(tmp != NULL){
				if(tmp->informacao.codigo == codigo){
					printf("\n%d - %s\n",tmp->informacao.codigo,tmp->informacao.nome);
					printf("\nData de Nascimento:%s",tmp->informacao.data);
					printf("\nSexo:%s",tmp->informacao.sexo);
					printf("\n\nTelefone:%s",tmp->informacao.telefone);
					printf("\nSenha:%s",tmp->informacao.senha);
					printf("\nUol Mail:%s",tmp->informacao.email);
				}
				tmp = tmp->prox;
			}
		}
		break;
	case 2:
		printf("\nGostaria de pesquisar por:\n1-Razão social.\n2-Codigo do mail.\n");
		printf("Escolha:");
		scanf("%d",&escolha);
		__fpurge(stdin);
		if(escolha == 1){
			printf("\nDigite a Razão Social a ser pesquisada:");
			LerString(c);
			__fpurge(stdin);
			int i = 0;
			int j = 0;
			while(c[i] != '\0'){
				i++;
			}
			int k = 0;
			while(tmp != NULL){
				j = 0;
				while(j < i){
					if(tmp->informacao.razaoSocial[k] == c[k])
						k++;
				j++;
				}
				if(k == i){
					printf("\n%d - %s\n",tmp->informacao.codigo,tmp->informacao.razaoSocial);
					printf("\nPessoa Jurídica:%s",tmp->informacao.pessoaJuridica);
					printf("\nCNPJ:%s",tmp->informacao.cnpj);
					printf("\n\nTelefone:%s",tmp->informacao.telefone);
					printf("\nSenha:%s",tmp->informacao.senha);
					printf("\nUol Mail:%s",tmp->informacao.email);
					break;
				}
				tmp = tmp->prox;
			}
		}
		else if(escolha == 2){
			int codigo;
			printf("\nDigite o codigo do mail desejado:");
			scanf("%d",&codigo);
			__fpurge(stdin);
			while(tmp != NULL){
				if(tmp->informacao.codigo == codigo){
					printf("\n%d - %s\n",tmp->informacao.codigo,tmp->informacao.razaoSocial);
					printf("\nPessoa Jurídica:%s",tmp->informacao.pessoaJuridica);
					printf("\nCNPJ:%s",tmp->informacao.cnpj);
					printf("\n\nTelefone:%s",tmp->informacao.telefone);
					printf("\nSenha:%s",tmp->informacao.senha);
					printf("\nUol Mail:%s s",tmp->informacao.email);
				}
				tmp = tmp->prox;
			}
		}			 	
		break;
	}
	
	printf("\n\n'Aperte a tecla [ENTER] para voltar ao menu.'\n");
	n = getchar();
	__fpurge(stdin);
}
void Remover(lista *l){
	no *tmp = l->inicio;
	no *p;
	printf("\nDigite o nome da pessoa que deseja remover o email:");
	char c[30];
	LerString(c);
	__fpurge(stdin);
	int i = 0;
	int j = 0;
	if(l->qtde == 0){
		l->inicio = NULL;
		l->final = NULL;
		free(tmp);
	}
	while(c[i] != '\0'){
		i++;
	}
	int k = 0;
	while(tmp != NULL){
		j = 0;
		while(j < i){
			if(tmp->informacao.nome[k] == c[k])
				k++;
		j++;
		}
		if(k == i){
			p = tmp;
			if(tmp == l->inicio){
				tmp->prox->ant = NULL;
				l->inicio = tmp->prox;
				free(p);
			}
			else if(tmp == l->final){
				tmp->ant->prox = NULL;
				l->final = tmp->ant;
				free(p);
			}
			else{
				tmp->ant->prox = tmp->prox;
				tmp->prox->ant = tmp->ant;
				free(p);	
			}
			break;
		}
		tmp = tmp->prox;
	}
	
}



void RemoverJuridico(lista *l){
	no *tmp = l->inicio;
	no *p;
	printf("\nDigite a Razão Social da empresa que deseja remover o email:");
	char c[30];
	LerString(c);
	__fpurge(stdin);
	int i = 0;
	int j = 0;
	if(l->qtde == 0){
		l->inicio = NULL;
		l->final = NULL;
		free(tmp);
	}
	while(c[i] != '\0'){
		i++;
	}
	int k = 0;
	while(tmp != NULL){
		j = 0;
		while(j < i){
			if(tmp->informacao.razaoSocial[k] == c[k])
				k++;
		j++;
		}
		if(k == i){
			p = tmp;
			if(tmp == l->inicio){
				tmp->prox->ant = NULL;
				l->inicio = tmp->prox;
				free(p);
			}
			else if(tmp == l->final){
				tmp->ant->prox = NULL;
				l->final = tmp->ant;
				free(p);
			}
			else{
				tmp->ant->prox = tmp->prox;
				tmp->prox->ant = tmp->ant;
				free(p);	
			}
			break;
		}
		tmp = tmp->prox;
	}
	
}

void MudarInfo(lista *l){
	int opcao;
	printf("Deseja mudar dados de uma:\n1-Pessoa Física.\n2-Pessoa Jurídica.\n");
	printf("Opção: ");
	scanf("%d",&opcao);
	switch(opcao)
	{
	case 1:
		printf("\nDigite o nome da pessoa que deseja fazer alterações:");
		char c[30];
		no *tmp = l->inicio;
		int escolha;
		LerString(c);
		__fpurge(stdin);
		char g;
		int i = 0;
		int j = 0;
		while(c[i] != '\0'){
			i++;
		}
		int k = 0;
		while(tmp != NULL){
			j = 0;
			while(j < i){
				if(tmp->informacao.nome[k] == c[k])
					k++;
			j++;
			}
			if(k == i){
				while(1){
					printf("\nQual campo deseja alterar:\n\n1-Nome.\n2-Data de Nascimento.\n3-Sexo.\n4-Telefone.\n5-Senha.\n6-Mail.\n");
					printf("->");
					scanf("%d",&escolha);
					__fpurge(stdin);
					if(escolha == 1){
						printf("\nDigite o novo nome desejado:");
						LerString(tmp->informacao.nome);
						__fpurge(stdin);
					}
					if(escolha == 2){
						printf("\nDigite a nova data de nascimento desejada:");
						fgets(tmp->informacao.data,10,stdin);
						__fpurge(stdin);
					}
					if(escolha == 3){
						printf("\nDigite o novo sexo:");
						fgets(tmp->informacao.sexo,10,stdin);
						__fpurge(stdin);
					}
					if(escolha == 4){
						printf("\nDigite o novo telefone:");
						fgets(tmp->informacao.telefone,9,stdin);
						__fpurge(stdin);
					}
					if(escolha == 5){
						printf("\nDigite a nova senha desejada:");
						fgets(tmp->informacao.senha,20,stdin);
						__fpurge(stdin);
						char confirmaSenha[20];
						printf("\nConfirme a senha:");
						fgets(confirmaSenha,20,stdin);
						__fpurge(stdin);
						int comp = strcmp(confirmaSenha, tmp->informacao.senha);
						while(comp!=0)
						{
							printf("A Senha não confere! Digite a senha novamente: ");
							fgets(tmp->informacao.senha,20,stdin);
							__fpurge(stdin);
							char confirmaSenha[20];
							printf("\nConfirme a senha:");
							fgets(confirmaSenha,20,stdin);
							__fpurge(stdin);
							comp = strcmp(confirmaSenha, tmp->informacao.senha);
						}
					}
					if(escolha == 6){
						printf("\nDigite o novo mail desejado:");
						fgets(tmp->informacao.email,30,stdin);
						__fpurge(stdin);
					}
				system("clear");
				printf("\nDeseja Alterar outro campo?\n\n'Digite [s] para ""sim"" e [n] para ""não""\n");
				scanf("%c",&g);
				__fpurge(stdin);
				if(g == 'n')
					break;
				}
				break;
			}
			tmp = tmp->prox;
		}
		break;
	case 2: 
		printf("\nDigite a Razão Social da empresa que deseja fazer alterações:");
		char c2[30];
		no *tmp2 = l->inicio;
		int escolha2;
		LerString(c);
		__fpurge(stdin);
		char g2;
		int i2 = 0;
		int j2 = 0;
		while(c2[i2] != '\0'){
			i++;
		}
		int k2 = 0;
		while(tmp2 != NULL){
			j2 = 0;
			while(j2 < i2){
				if(tmp2->informacao.razaoSocial[k2] == c2[k])
					k2++;
			j2++;
			}
			if(k2 == i2){
				while(1){
					printf("\nQual campo deseja alterar:\n\n1-Pessoa Jurídica.\n2-Razão Social.\n3-CNPJ.\n4-Telefone.\n5-Senha.\n6-Mail.\n");
					printf("->");
					scanf("%d",&escolha2);
					__fpurge(stdin);
					if(escolha2 == 1){
						printf("\nDigite a nova Pessoa Jurídica desejada:");
						LerString(tmp2->informacao.pessoaJuridica);
						__fpurge(stdin);
					}
					if(escolha2 == 2){
						printf("\nDigite a nova Razão Social desejada:");
						fgets(tmp2->informacao.data,10,stdin);
						__fpurge(stdin);
					}
					if(escolha2 == 3){
						printf("\nDigite o novo CNPJ:");
						fgets(tmp2->informacao.sexo,14,stdin);
						__fpurge(stdin);
					}
					if(escolha2 == 4){
						printf("\nDigite o novo telefone:");
						fgets(tmp2->informacao.telefone,9,stdin);
						__fpurge(stdin);
					}
					if(escolha2 == 5){
						printf("\nDigite a nova senha desejada:");
						fgets(tmp2->informacao.senha,20,stdin);
						__fpurge(stdin);
						char confirmaSenha[20];
						printf("\nConfirme a senha:");
						fgets(confirmaSenha,20,stdin);
						__fpurge(stdin);
						int comp = strcmp(confirmaSenha, tmp2->informacao.senha);
						while(comp!=0)
						{
							printf("A Senha não confere! Digite a senha novamente: ");
							fgets(tmp2->informacao.senha,20,stdin);
							__fpurge(stdin);
							char confirmaSenha[20];
							printf("\nConfirme a senha:");
							fgets(confirmaSenha,20,stdin);
							__fpurge(stdin);
							comp = strcmp(confirmaSenha, tmp2->informacao.senha);
						}
					}
					if(escolha2 == 6){
						printf("\nDigite o novo mail desejado:");
						fgets(tmp2->informacao.email,30,stdin);
						__fpurge(stdin);
					}
				system("clear");
				printf("\nDeseja Alterar outro campo?\n\n'Digite [s] para ""sim"" e [n] para ""não""\n");
				scanf("%c",&g);
				__fpurge(stdin);
				if(g2 == 'n')
					break;
				}
				break;
			}
			tmp2 = tmp2->prox;
		}
		break;
	}
}
void RemoverInicio(lista *l){
	no *tmp = l->inicio;
	if(l->inicio == l->final){
		l->inicio = NULL;
		l->final = NULL;
	}
	else{
		l->inicio->prox->ant = NULL;
		l->inicio = l->inicio->prox;	
	}
	free(tmp);
}
